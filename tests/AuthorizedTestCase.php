<?php

namespace Tests;


use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

abstract class AuthorizedTestCase extends TestCase
{
    protected $token;

    abstract protected function getScopes(): array;

    protected function getAuthorizationHeader(): array
    {
        return ['Authorization' => 'Bearer ' . $this->token];
    }

    protected function setUp()
    {
        parent::setUp();

        config()->set('oauth2.public_key', $this->getPublicKey());
        config()->set('oauth2.audience', 'http://meetup');
        config()->set('oauth2.issuer', 'https://authstack');

        $this->createOAuth2Token();

        request()->headers->add([
            'Authorization' => 'Bearer ' . $this->token
        ]);
    }

    protected function createOAuth2Token()
    {
        $builder = new Builder();

        $expires = time() + 3600;

        $builder
            ->setIssuer('https://authstack')
            ->setSubject('system-user')
            ->setAudience('http://meetup')
            ->setExpiration($expires)
            ->setIssuedAt(time())
        ;

        //$scopes = ['client:create', 'client:read', 'client:update', 'client:delete'];
        $scopes = $this->getScopes();

        $builder->set('scope', implode(' ', $scopes));

        $signer = Sha256::class;

        $builder->sign(new $signer, new Key($this->getPrivateKey()));

        $this->token = (string)$builder->getToken();
    }

    protected function getPrivateKey()
    {
        return <<<EOF
-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDUCZG3fkE2P7m7
mA8EIglhu2u5YgOUFUUDy7k0SjxOdVS3mhZ/bqXzHIWd5VQQJ0Rq/6C/qcjYsqbi
4zkymiN3YI8VtBY0zeh2W68wOtMEJFKeb+CLgoRdjiejc/MAqm6Pb54FLiuxb4Dy
rSxUUFe3t2zC36wWcPxt30tLuUbyOb20ho5soE0J2CKChurW9Kj9bsNnF+ZBFIHd
ozScx8R1OxkQihHRn9wLvLTJP1ZkgK81ssE5xkteYZKYyVMhuFOWehYJf93ZoQIf
pEnjMZvo7pgvZoS46x5VqnrdZC+BW83emQM31RJFRKJ+zJRAorqOkiDx2Z5J/MyV
I35QbYAVAgMBAAECggEAXTpD3mjdpUuJj0jBOXD5rQXpzF2i8yYS7SNSsAH/2ANQ
l1VhlPACeQzOVukw99U8CMuSUxMEovqR0+pSCMSq2V8UKle+98Kc9gVD1rHWe9i4
lr7b/JsLNdM97Gkbq+xDpj9OPDrbrS04hfrMJdoZZ0Z3xIrxsABHjMuD4Kx63B7n
mfVsv0WfC/896UCJqm/UQbZthxn5XzE3rZVLLIcXbP+xZQ8WkCvn7jm1Dp0sXN+o
TuK3P9wEPBeDL3lENVvBXWUYqhwbob8GPs58W9p6E6MO3ZlMk2iHohuh+qQqIre6
LnfbIbCVK5t1zcaYf1IeSktwNxAG1KvGJLXb/b+Z4QKBgQD8S4E+/I+uMmlN7PCM
lLYBd+ScmGQGslZ/ePNHV8Pyym3XFPNvMnEwxCGzQo1q/p4tikz6YlXrioCQiVSy
l9Fpt36da8r+qxJKY+zaOee37xEjPFfsGkQdSAHUkT9CwzLklbn6xxGjGoNPmSKt
joB5fl+pBWGGr+tCXQILRrg5/QKBgQDXJreeu4QD69Ha7qOkWxbxx8ZOBtjBq2E9
tNddwTt4btBvT/5V8H7rcNBcwXroi7R1MPIRIGu6HhCUr7S0BnNnIP9F6WcHux5S
r2fFAB/3YNUcLAxPtY4Xn2KNT/1mkuB813RT4XsHXJKP8prARatVN847T5ImkxI8
CtazKLxN+QKBgBTD2iVnACRkPcqOH9ZSPxCr34ML/9+VOg1P1SYgER6gjp3POaID
UtGisutgJOeTgU1aPq+0/EFgqug6hWAVosTCt4cZYiHHzr+Urd/gODQpVw3BX7Mt
DkIitRPbyIskKcIqmJ4NWGRGQFyCzBngyq41OoZczQQa4pAZu5tV/L+5AoGAERgT
aLovT/xxjPp+5/UQyp77w4FJwYFzVWrD3cAXkIcFaoKpi3xzKROqimQY9tb6vIDq
CrBPvtbarGWVbuRAUQZPIefcdsHIM/uZvA45e/cml3lsdPzQ0FGqi926HWc4DNM1
e+CjXL9fKa612ubuKPQpH48/5m6y+TBHRK7M30kCgYAEV6MNcs5clnYwPC4LmcBb
nrdPNKTqKmlIeCxbr2RQU0Sp2XeJAOX/PRTcyEdJXr4sNocfWfy4llsoYlLlnvKu
Y0gxU8m49L1q6yw3GiuvSHAmQ4kJzjozi2Gj1zkANhgHiAPKghvEn8wVUO6WeIik
hgEata8SMVWKvT8RiQJ0LQ==
-----END PRIVATE KEY-----
EOF;

    }

    protected function getPublicKey()
    {
        return <<<EOF
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1AmRt35BNj+5u5gPBCIJ
YbtruWIDlBVFA8u5NEo8TnVUt5oWf26l8xyFneVUECdEav+gv6nI2LKm4uM5Mpoj
d2CPFbQWNM3odluvMDrTBCRSnm/gi4KEXY4no3PzAKpuj2+eBS4rsW+A8q0sVFBX
t7dswt+sFnD8bd9LS7lG8jm9tIaObKBNCdgigobq1vSo/W7DZxfmQRSB3aM0nMfE
dTsZEIoR0Z/cC7y0yT9WZICvNbLBOcZLXmGSmMlTIbhTlnoWCX/d2aECH6RJ4zGb
6O6YL2aEuOseVap63WQvgVvN3pkDN9USRUSifsyUQKK6jpIg8dmeSfzMlSN+UG2A
FQIDAQAB
-----END PUBLIC KEY-----
EOF;

    }
}
