<?php

namespace Tests\Feature;

use Tests\AuthorizedTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TodoTest extends AuthorizedTestCase
{
    public function testCreate()
    {

    }

    protected function getScopes(): array
    {
        return ['todo:read', 'todo:create', 'todo:update', 'todo:delete'];
    }
}
