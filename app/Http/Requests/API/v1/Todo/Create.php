<?php

namespace App\Http\Requests\API\v1\Todo;

use function App\Functions\can;
use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    public function authorize()
    {
        return can('todo:read');
    }

    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:65536',
            'due_at' => 'required|date_format:Y-m-d H:i'
        ];
    }
}
