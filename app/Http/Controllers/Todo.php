<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\v1\Todo\Create;
use App\Http\Requests\API\v1\Todo\Read;
use App\Http\Requests\API\v1\Todo\Update;
use App\Http\Requests\API\v1\Todo\Delete;

class Todo extends Controller
{
    public function create(Create $request)
    {}

    public function listing(Read $request)
    {}

    public function read(Read $request, $id)
    {}

    public function update(Update $request, $id)
    {}

    public function delete(Delete $request, $id)
    {}
}
