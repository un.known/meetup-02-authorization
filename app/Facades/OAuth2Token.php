<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class OAuth2Token extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \App\Services\Authorization\OAuth2Token::class;
    }
}