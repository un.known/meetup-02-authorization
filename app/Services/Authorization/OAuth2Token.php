<?php

namespace App\Services\Authorization;

use Lcobucci\JWT\Token;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer;
use App\Services\Authorization\Exception\AuthorizationException;

class OAuth2Token
{
    protected $jwt;
    protected $token;
    protected $signer;
    protected $pub_key;
    protected $issuer;
    protected $audience;

    public function __construct(string $token = null, Signer $signer, string $pub_key, string $issuer, string $audience)
    {
        $this->jwt = $token;

        $this->signer = $signer;

        $this->pub_key = $pub_key;

        $this->issuer = $issuer;

        $this->audience = $audience;

        try
        {
            $this->token = (new Parser)->parse($token);

            $valid = $this->token->verify($this->signer, $this->pub_key);

            // Verify the signature
            if(!$valid)
            {
                throw new \Exception('Invalid signature');
            }

            // Verify that the token is issued by valid idp
            if(0 !== strcmp($this->token->getClaim('iss'), $this->issuer))
            {
                throw new \Exception(sprintf('Invalid issuer: %s', $this->token->getClaim('iss')));
            }

            // Verify that the audience is valid (that we're the intended recipient)
            if(0 !== strcmp($this->token->getClaim('aud'), $this->audience))
            {
                throw new \Exception(sprintf('Invalid audience: %s', $this->token->getClaim('aud')));
            }

            // Finally, check if the token has expired
            if($this->token->isExpired())
            {
                throw new \Exception('Token expired');
            }
        }
        catch(\Exception $e)
        {
            AuthorizationException::error($e->getMessage());
        }
    }

    /**
     * @param string $scope
     * @return bool
     */
    public function can(string $scope): bool
    {
        return $this->hasScope($scope);
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->getToken()->isExpired();
    }

    /**
     * @return null|string
     */
    public function getUserID(): ?string
    {
        return $this->getToken()->getClaim('sub');
    }

    /**
     * @return Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * @param string $scope
     * @return bool
     */
    public function hasScope(string $scope): bool
    {
        return in_array($scope, $this->getScopes(), true);
    }

    /**
     * @return array
     */
    public function getScopes(): array
    {
        return explode(' ', $this->getToken()->getClaim('scope', ''));
    }
}