<?php

namespace App\Services\Authorization\Exception;

use Illuminate\Http\JsonResponse;

class AuthorizationException extends \Exception
{
    const E_INVALID_REQUEST = 'invalid_request';
    const E_UNAUTHORIZED_CLIENT = 'unauthorized_client';
    const E_INVALID_CLIENT = 'invalid_client';
    const E_ACCESS_DENIED  = 'access_denied';
    const E_UNSUPPORTED_RESP_TYPE = 'unsupported_response_type';
    const E_INVALID_SCOPE = 'invalid_scope';
    const E_SERVER_ERROR = 'server_error';
    const E_TEMPORARILY_UNAVAILABLE = 'temporarily_unavailable';

    protected $error_code;
    protected $error_description;
    protected $error_uri;
    protected $status_code;

    public function __construct(string $error_code, string $error_description = '', string $error_uri = '',  string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->error_code = $error_code;
        $this->error_description = $error_description;
        $this->error_uri = $error_uri;
        $this->status_code = $code;
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json(['error' => $this->getOAuthErrorCode(), 'error_description' => $this->getErrorDescription()], $this->getStatusCode());
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        $code = (int)$this->status_code;

        if(empty($code))
        {
            $code = 500;
        }

        return $code;
    }

    /**
     * @return string
     */
    public function getOAuthErrorCode(): string
    {
        return $this->error_code;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->error_description;
    }

    /**
     * @return string
     */
    public function getErrorUri(): string
    {
        return $this->error_uri;
    }

    /**
     * @param string $description
     * @throws AuthorizationException
     */
    public static function error(string $description = 'An error occurred')
    {
        throw new self(self::E_SERVER_ERROR, $description, '', '', 403);
    }

    /**
     * @param string $description
     * @throws AuthorizationException
     */
    public static function unauthorized(string $description = 'Authorization missing')
    {
        throw new self(self::E_UNAUTHORIZED_CLIENT, $description, '', '', 403);
    }
}