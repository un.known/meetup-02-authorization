<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Authorization\OAuth2Token;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OAuth2Token::class, function()
        {
            $signer = config('oauth2.signer');
            $pub_key = config('oauth2.public_key');
            $issuer = config('oauth2.issuer');
            $audience = config('oauth2.audience');
            $token = trim(str_replace('Bearer ', '', request()->header('Authorization')));

            return new OAuth2Token($token, new $signer, $pub_key, $issuer, $audience);
        });
    }
}
