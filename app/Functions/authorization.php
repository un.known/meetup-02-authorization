<?php

namespace App\Functions;

use App\User;
use App\Facades\OAuth2Token;

/**
 * @param string $scope
 * @return bool
 */
function can(string $scope): bool
{
    return OAuth2Token::can($scope);
}
