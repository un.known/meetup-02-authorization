<?php

Route::get('/create',   'Todo@create');
Route::get('/read',     'Todo@read');
Route::get('/update',   'Todo@update');
Route::get('/delete',   'Todo@delete');