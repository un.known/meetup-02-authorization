<?php

return [
    'signer' => \Lcobucci\JWT\Signer\Rsa\Sha256::class,

    'public_key' => <<<EOF
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1AmRt35BNj+5u5gPBCIJ
YbtruWIDlBVFA8u5NEo8TnVUt5oWf26l8xyFneVUECdEav+gv6nI2LKm4uM5Mpoj
d2CPFbQWNM3odluvMDrTBCRSnm/gi4KEXY4no3PzAKpuj2+eBS4rsW+A8q0sVFBX
t7dswt+sFnD8bd9LS7lG8jm9tIaObKBNCdgigobq1vSo/W7DZxfmQRSB3aM0nMfE
dTsZEIoR0Z/cC7y0yT9WZICvNbLBOcZLXmGSmMlTIbhTlnoWCX/d2aECH6RJ4zGb
6O6YL2aEuOseVap63WQvgVvN3pkDN9USRUSifsyUQKK6jpIg8dmeSfzMlSN+UG2A
FQIDAQAB
-----END PUBLIC KEY-----
EOF
,

    'audience' => 'http://meetup',

    'issuer' => 'https://authstack'
];
